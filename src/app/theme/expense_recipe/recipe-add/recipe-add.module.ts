import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipeAddComponent } from './recipe-add.component';
import {RecipeAddRoutingModule} from './recipe-add-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {SelectModule} from 'ng-select';
import {FormsModule} from '@angular/forms';
import {ToastyModule} from 'ng2-toasty';

@NgModule({
  imports: [
    CommonModule,
    RecipeAddRoutingModule,
    SharedModule,
    Ng2GoogleChartsModule,
    SelectModule,
    FormsModule,
    ToastyModule.forRoot()
  ],
  declarations: [RecipeAddComponent]
})
export class RecipeAddModule { }
