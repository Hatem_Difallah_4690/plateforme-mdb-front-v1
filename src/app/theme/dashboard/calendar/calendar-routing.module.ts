import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CalendarComponent} from './calendar.component';

const routes: Routes = [
  {
    path: '',
    component: CalendarComponent,
    data: {
      title: 'Calendrier budgétaire',
      icon: 'icon-home',
      caption: 'Cette page affiche tous les détails de calendrier budgétaire - Calendrier budgétaire ',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule { }
