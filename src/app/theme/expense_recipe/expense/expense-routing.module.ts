import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ExpenseComponent} from './expense.component';

const routes: Routes = [
  {
    path: '',
    component: ExpenseComponent,
    data: {
      title: 'Dépense d\'ètat',
      icon: 'icon-bar-chart-alt',
      caption: 'Cette page affiche le détails du dépense d\'ètat - Dépense d\'ètat',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseRoutingModule { }
