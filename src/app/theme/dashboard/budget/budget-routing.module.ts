import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BudgetComponent} from './budget.component';

const routes: Routes = [
  {
    path: '',
    component: BudgetComponent,
    data: {
      title: 'Budget de l\'état',
      icon: 'icon-home',
      caption: 'Cette page affiche tous les détails budget - Budget Etat',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetRoutingModule { }
