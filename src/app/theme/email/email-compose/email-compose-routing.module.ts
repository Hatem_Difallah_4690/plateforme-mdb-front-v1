import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EmailComposeComponent} from './email-compose.component';

const routes: Routes = [
  {
    path: '',
    component: EmailComposeComponent,
    data: {
      title: 'Email',
      icon: 'icon-home',
      caption: 'Compose Email',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailComposeRoutingModule { }
