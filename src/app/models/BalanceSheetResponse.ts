
export class BalanceSheetResponse {
  totalReceipt: string;
  totalExpense: string;
  sold: string;
}
