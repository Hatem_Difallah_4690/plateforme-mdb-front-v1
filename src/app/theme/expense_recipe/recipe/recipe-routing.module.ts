import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecipeComponent} from './recipe.component';

const routes: Routes = [
  {
    path: '',
    component: RecipeComponent,
    data: {
      title: 'Recette d\'ètat',
      icon: 'icon-bar-chart-alt',
      caption: 'Cette page affiche le détails du recette d\'ètat - Recette d\'ètat',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipeRoutingModule { }
