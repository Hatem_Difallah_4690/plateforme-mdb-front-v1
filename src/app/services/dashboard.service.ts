import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {BalanceSheetResponse} from '../models/BalanceSheetResponse';
import {environment} from '../../environments/environment';
import {ErrorService} from './error.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private baseUrl: string = environment.urlBackMdb + '/api/v0';

  constructor(private http: HttpClient, private errorService: ErrorService) {
  }

  /***
   * getCalendarBudget
   *
   */
  getCalendarBudget(): Observable<any> {
    return this.http.get<BalanceSheetResponse>(`${this.baseUrl}/calendar_budget`)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }


}
