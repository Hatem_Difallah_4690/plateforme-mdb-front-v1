import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FileUploadUiComponent} from './file-upload-ui.component';

const routes: Routes = [
  {
    path: '',
    component: FileUploadUiComponent,
    data: {
      title: 'Téléchargement de fichier',
      icon: 'icon-layers',
      caption: 'Cette page utiliser pour uploader des fichiers - Téléchargement de fichier',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileUploadUiRoutingModule { }
