import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipeComponent } from './recipe.component';
import {RecipeRoutingModule} from './recipe-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {SelectModule} from 'ng-select';
import {FormsModule} from '@angular/forms';
import {ToastyModule} from 'ng2-toasty';
import {NgxEchartsModule} from 'ngx-echarts';

@NgModule({
  imports: [
    CommonModule,
    RecipeRoutingModule,
    SharedModule,
    Ng2GoogleChartsModule,
    SelectModule,
    FormsModule,
    NgxEchartsModule,
  ],
  declarations: [RecipeComponent]
})
export class RecipeModule { }
