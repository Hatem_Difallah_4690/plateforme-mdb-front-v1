import {Component,  OnInit, ViewEncapsulation} from '@angular/core';

declare const AmCharts: any;

import '../../../../assets/charts/amchart/amcharts.js';
import '../../../../assets/charts/amchart/gauge.js';
import '../../../../assets/charts/amchart/pie.js';
import '../../../../assets/charts/amchart/serial.js';
import '../../../../assets/charts/amchart/light.js';
import '../../../../assets/charts/amchart/ammap.js';
import '../../../../assets/charts/amchart/usaLow.js';
import {BudgetService} from '../../../services/budget.service';
import {ReceiptsService} from '../../../services/receipts.service';
import {ExpensesService} from '../../../services/expenses.service';
import {BalanceSheetResponse} from '../../../models/BalanceSheetResponse';
import Chart from 'chart.js';


@Component({
  selector: 'app-default',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BudgetComponent implements OnInit{
  options: any = {
    position: ['bottom', 'right'],
  };


  // chart variables
  public canvas: any;
  public ctx;
  public chartColor;
  public chartExpensesByNature;
  public chartExpensesByArticles;
  public donutChartData: any = {} ;
  // prices variables
  public sold: string ;
  public checkSoldForColor: number ;
  public totalReceipt: string;
  public totalExpense: string;

  constructor(private budgetService: BudgetService ,
              private receiptsService: ReceiptsService ,
              private expensesService: ExpensesService,
  ) {
  }




  ngOnInit(): void {

    // getBalanceSheet
    this.getBalanceSheet() ;

    // buildChartExpenseByNature
    this.buildChartExpenseByNature();

    // buildChartReceiptByArticle
    this. buildChartReceiptByArticle();


  }

  /****
   * getBalanceSheet
   *
   */
  getBalanceSheet(): void {
    const convert = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'CFA', });
    this.budgetService.getBalanceSheet().subscribe((data: BalanceSheetResponse) => {
      if (data !== undefined) {
        this.checkSoldForColor = Number(data.sold ) ;
        this.sold = convert.format(Number(data.sold));
        this.totalReceipt = convert.format(Number(data.totalReceipt));
        this.totalExpense = convert.format(Number(data.totalExpense));
      }
    });
  }

  /***
   * buildChartExpenseByNature
   *
   */
  buildChartExpenseByNature(): void {

    this.expensesService.getAllExpensesByNature().subscribe((data: any) => {

      const listNature = [];
      const listExpense = [];

      data.forEach((obj: any) => {
        listNature.push(obj.nature);
        listExpense.push(obj.percentage);

      });

      this.canvas = document.getElementById('chartExpensesByNature');
      this.ctx = this.canvas.getContext('2d');
      this.chartExpensesByNature = new Chart(this.ctx, {
        type: 'pie',
        data: {
          labels: listNature,
          datasets: [{
            pointRadius: 0,
            pointHoverRadius: 0,
            backgroundColor: [
              '#869a04',
              '#9c2828',
              '#46cdd2',
              '#1f9539',
              '#0a3599',
              '#8526b8',
            ],
            borderWidth: 0,
            data: listExpense
          }]
        },

        options: {

          legend: {
            display: true
          },

          pieceLabel: {
            render: 'percentage',
            fontColor: ['white'],
            precision: 2
          },

          tooltips: {
            enabled: true
          },

          scales: {
            yAxes: [{

              ticks: {
                display: false
              },
              gridLines: {
                drawBorder: false,
                zeroLineColor: 'transparent',
                color: 'rgba(255,255,255,0.05)'
              }

            }],

            xAxes: [{
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: 'rgba(255,255,255,0.1)',
                zeroLineColor: 'transparent'
              },
              ticks: {
                display: false,
              }
            }]
          },
        }
      });

    });
  }

  /**
   * buildChartReceiptByArticle
   *
   */
  buildChartReceiptByArticle(): void {

    this.receiptsService.getAllReceiptsByArticles().subscribe((data: any) => {

      const listArticle = [];
      const listReceipt = [];

      data.forEach((obj: any) => {
        listArticle.push(obj.article);
        listReceipt.push(obj.percentage);

      });

      this.canvas = document.getElementById('chartReceiptsByArticles');
      this.ctx = this.canvas.getContext('2d');
      this.chartExpensesByArticles = new Chart(this.ctx, {
        type: 'pie',
        data: {
          labels: listArticle,
          datasets: [{
            pointRadius: 0,
            pointHoverRadius: 0,
            backgroundColor: [
              '#fad62d',
              '#9c2828',
              '#46cdd2',
              '#09bb12',
              '#0a3599',
              '#8526b8',
            ],
            borderWidth: 0,
            data: listReceipt
          }]
        },

        options: {

          legend: {
            display: true
          },

          pieceLabel: {
            render: 'percentage',
            fontColor: ['white'],
            precision: 2
          },

          tooltips: {
            enabled: true
          },

          scales: {
            yAxes: [{

              ticks: {
                display: false
              },
              gridLines: {
                drawBorder: false,
                zeroLineColor: 'transparent',
                color: 'rgba(255,255,255,0.05)'
              }

            }],

            xAxes: [{
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: 'rgba(255,255,255,0.1)',
                zeroLineColor: 'transparent'
              },
              ticks: {
                display: false,
              }
            }]
          },
        }
      });



    });
  }


}
