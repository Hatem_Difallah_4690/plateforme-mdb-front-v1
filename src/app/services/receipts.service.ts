import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {BalanceSheetResponse} from '../models/BalanceSheetResponse';
import {environment} from '../../environments/environment';
import {ErrorService} from './error.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' ,
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'})
};

@Injectable({
  providedIn: 'root'
})
export class ReceiptsService {

  private baseUrl: string = environment.urlBackMdb + '/api/v0/receipts';

  private baseUrl2: string = environment.urlBackImport + '/api/v0/receipt';

  constructor(private http: HttpClient, private errorService: ErrorService) {
  }

  /***
   * getAllReceiptsByArticles
   *
   */
  getAllReceiptsByArticles(): Observable<any> {
    return this.http.get<BalanceSheetResponse>(`${this.baseUrl}/articles`)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

  /**
   * getDetailsArticle
   *
   * @param article
   */
  getDetailsArticle(article: string): Observable<any> {

    return this.http.get(`${this.baseUrl}/article/` + article)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

  /**
   * findParagraphesByArticle
   *
   */
  findParagraphesByArticle(article): Observable<any> {
    return this.http.post(`${this.baseUrl2}/paragraphe` , article , httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

  /**
   * findSousParagraphesByParagraphe
   *
   */
  findSousParagraphesByParagraphe(paragraphe): Observable<any> {
    return this.http.post<any>(`${this.baseUrl2}/sous/paragraphe` , paragraphe, httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

  /**
   * findRubricsBySousParagraphe
   *
   */
  findRubricsBySousParagraphe(sousParagraphe): Observable<any> {
    return this.http.post<any>(`${this.baseUrl2}/rubric` , sousParagraphe, httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }
  /**
   * findSousRubricsByRubric
   *
   */
  findSousRubricsByRubric(rubric): Observable<any> {
    return this.http.post<any>(`${this.baseUrl2}/sous/rubric` , rubric, httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

  /**
   * findRepartitionsBySousRubric
   *
   */
  findRepartitionsBySousRubric(sousRubric): Observable<any> {
    return this.http.post<any>(`${this.baseUrl2}/repartition` , sousRubric, httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }


  /**
   * createReceipt
   *
   * @param recette
   */
  createReceipt(recette): Observable<any> {

    return this.http.post<any>(`${this.baseUrl2}/add` , recette , httpOptions  ).pipe(
      retry(1),
      catchError(this.errorService.httpError)
    )
  }


}
