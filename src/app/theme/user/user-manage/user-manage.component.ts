import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user.service';
import {ToastyService} from 'ng2-toasty';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: [
    './user-manage.component.scss'
  ]
})
export class UserManageComponent implements OnInit {

  public listUsers = [];
  public user: any;
  public event: any;

  constructor(
    private userService: UserService ,
    private toastyService: ToastyService
  ) { }


  ngOnInit(): void {
    this.getAllUsers();
  }


  getAllUsers(): void {
    this.userService.getAllUsers().subscribe((data: any) => {
      this.listUsers = data;
    });
  }


  deleteUser(): void {
    const x = document.getElementById('effect-2');
    this.userService.deleteUser(this.user.userId).subscribe((data: any) => {
      this.userService.getAllUsers().subscribe((users: any) => {
        this.listUsers = users;
        this.showSuccess('Success' , 'User deleted successfully');
        x.classList.remove('md-show');
      });
    });
  }


  updateUser(): void {

    const x = document.getElementById('effect-3');
    this.user.active = !this.user.active;
    this.userService.updateUser(this.user).subscribe(() => {
      this.userService.getAllUsers().subscribe((users: any) => {
        this.listUsers = users;
        this.showSuccess('Success' , 'User updated successfully' );
        x.classList.remove('md-show');
      });

    });

  }

  openMyModal(event , dialogId , user ): void {
    document.querySelector('#' + dialogId).classList.add('md-show');
    this.user = user ;
    this.event = event;
    console.log(  this.event );

  }


  closeMyModal(event): void {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }


  showSuccess(title , msg): void {
    this.toastyService.success(
      { title , msg   , timeout: 5000 , showClose: true , theme: 'bootstrap'
      });
  }
}
