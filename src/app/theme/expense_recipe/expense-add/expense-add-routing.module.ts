import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ExpenseAddComponent} from './expense-add.component';

const routes: Routes = [
  {
    path: '',
    component: ExpenseAddComponent,
    data: {
      title: 'Ajouter une Dépense',
      icon: 'icon-bar-chart-alt',
      caption: 'Cette page affiche la formulaire de saissie  d\'une nouvelle Dépense - Ajouter une Dépense',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseAddRoutingModule { }
