import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {BalanceSheetResponse} from '../models/BalanceSheetResponse';
import {environment} from '../../environments/environment';
import {ErrorService} from './error.service';

@Injectable({
    providedIn: 'root'
})
export class BudgetService {

    private baseUrl: string = environment.urlBackMdb + '/api/v0/budget';

  constructor(private http: HttpClient, private errorService: ErrorService) {
  }

  /***
   * getBalanceSheet
   *
   */
  getBalanceSheet(): Observable<BalanceSheetResponse> {
    return this.http.get<BalanceSheetResponse>(`${this.baseUrl}/balancesheet`)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

}
