import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {ErrorService} from './error.service';

@Injectable({
  providedIn: 'root'
})
export class ExpensesService {

  private baseUrl: string = environment.urlBackMdb + '/api/v0/expenses';

  constructor(private http: HttpClient, private errorService: ErrorService) {
  }


  /***
   * getAllExpensesByNature
   *
   */
  getAllExpensesByNature(): Observable<any> {
    return this.http.get(`${this.baseUrl}/natures`)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

  /***
   * getAllEntities
   *
   */
  getAllEntities(): Observable<any> {
    return this.http.get(`${this.baseUrl}/entities`)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

  /**
   * getAllExpensesByEntity
   *
   * @param entity
   */
  getAllExpensesByEntity(entity: string): Observable<any> {

    return this.http.get(`${this.baseUrl}/entity/` + entity)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      )
  }

}
