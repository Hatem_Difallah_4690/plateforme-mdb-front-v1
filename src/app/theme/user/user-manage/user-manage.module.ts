import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserManageComponent } from './user-manage.component';
import {UserManageRoutingModule} from './user-manage-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {ToastyModule} from 'ng2-toasty';

@NgModule({
  imports: [
    CommonModule,
    UserManageRoutingModule,
    SharedModule,
    ToastyModule.forRoot()
  ],
  declarations: [UserManageComponent]
})
export class UserManageModule { }
