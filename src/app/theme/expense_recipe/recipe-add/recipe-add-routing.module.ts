import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecipeAddComponent} from './recipe-add.component';

const routes: Routes = [
  {
    path: '',
    component: RecipeAddComponent,
    data: {
      title: 'Ajouter une Recette',
      icon: 'icon-bar-chart-alt',
      caption: 'Cette page affiche la formulaire de saissie  d\'une nouvelle Recette - Ajouter une Recette',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipeAddRoutingModule { }
