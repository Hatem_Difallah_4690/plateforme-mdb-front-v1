import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {ErrorService} from './error.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' ,
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'})
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = environment.urlBackMdb + '/api/v0/user';

  constructor(
    private http: HttpClient,
    private errorService: ErrorService) {
  }

  /**
   * authentication
   *
   * @param user
   */
  authentication(user): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/authentication` , user , httpOptions  ).pipe(
      retry(1),
      catchError(this.errorService.httpError)
    )
  }

  /**
   * createUser
   *
   * @param user
   */
  createUser(user): Observable<any> {

    return this.http.post<any>(`${this.baseUrl}/create` , user , httpOptions  ).pipe(
      retry(1),
      catchError(this.errorService.httpError)
    )
  }

  /**
   * updateUser
   *
   * @param user
   */
  updateUser(user): Observable<any> {

    return this.http.put<any>(`${this.baseUrl}/update` , user , httpOptions  ).pipe(
      retry(1),
      catchError(this.errorService.httpError)
    )
  }

  /**
   * deleteUser
   *
   * @param userId
   */
  deleteUser(userId): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/delete` , userId ).pipe(
      retry(1),
      catchError(this.errorService.httpError)
    )
  }

  /***
   * getAllUsers
   *
   */
  getAllUsers(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/users`)
      .pipe(
        retry(1),
        catchError(this.errorService.httpError)
      );
  }


  getUserByLogin(login): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/getUserByLogin` , login ).pipe(
      retry(1),
      catchError(this.errorService.httpError)
    );
  }
}
