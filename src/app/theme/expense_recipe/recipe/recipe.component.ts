import { Component, OnInit } from '@angular/core';
import {ReceiptsService} from '../../../services/receipts.service';
import {ThemeOption} from 'ngx-echarts';
import {ObjectChart} from '../../../models/ObjectsChart';
import {NgxUiLoaderService} from 'ngx-ui-loader';


@Component({
  selector: 'app-google-chart',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {



  // chart variables
  public chartExpensesByArticles: {};
  public chartParagraph: {};
  public theme: string | ThemeOption;

  // Receipt variables
  public listReceiptByArticles = [];
  public listReceiptBySousParagraph = [];
  public listReceiptBySousParagraph2 = [];
  public showDetailsArticle: boolean;
  public colors = [];

  // columnDefs
  columnDefs = [
    { field: 'name', sortable: true, filter: true , width: 800, suppressSizeToFit: true   },
    { field: 'value', sortable: true, filter: true  },
  ];

  // gridOptions
  gridOptions = {
    rowClass: 'my-green-class',
    getRowClass: params => {
      if (params.node.rowIndex % 2 === 0) {
        return 'my-shaded-effect';
      }
    },
  };


  constructor(private receiptsService: ReceiptsService , private ngxService: NgxUiLoaderService) { }

  ngOnInit(): void {
    this.colors = ['rgb(11,120,215)' , 'rgb(86,199,99)' , 'rgb(241,150,6)' , 'rgb(60,196,234)' , 'rgb(217,73,106)'];
    this.getAllReceiptsByArticles();
  }

  /***
   * getAllReceiptsByArticles
   *
   */
  getAllReceiptsByArticles(): void {
    this.ngxService.start();
    this.receiptsService.getAllReceiptsByArticles().subscribe((data: any) => {
      this.ngxService.stop();
      const listArticles = [];
      const listPercentages = [];

      data.forEach((obj: any) => {

        const objectChart = new ObjectChart();
        objectChart.name = obj.article ;
        objectChart.value = obj.sumReceipt ;

        this.listReceiptByArticles.push(objectChart);
        listArticles.push(obj.article);
        listPercentages.push(obj.percentage);
      });

      this.chartExpensesByArticles = {

        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
          x: 'center',
          y: 'bottom',
          data: listArticles
        },
        calculable: true,
        series: [
          {
            name: 'NATURE:',
            type: 'pie',
            radius: [30, 110],
            roseType: 'area',
            data:  this.listReceiptByArticles,
          }
        ],
      };

    }) ;



  }


  /***
   * getDetailsArticle
   *
   * */
  getDetailsArticle($event): void {

    console.log($event.color);
    this.ngxService.start();

    this.receiptsService.getDetailsArticle($event.data.name).subscribe((data: any) => {
      this.ngxService.stop();
      this.listReceiptBySousParagraph = data.receiptBySousParagraphe ;
      this.showDetailsArticle = true ;

      const yAxis = [];
      for (let i = 0; i < data.lengh ; i++) {
        yAxis.push(data.name);
      }

      this.chartParagraph = {
        legend: {
          data: ['Paragraphe'],
          align: 'left',
        },
        tooltip: {},
        xAxis: {},
        yAxis: {data: yAxis,
          silent: false,
          splitLine: {
            show: false,
          }
        },
        series: [
          {
            name: 'Paragraphe',
            type: 'bar',
            data: data.receiptByParagraphe,
            animationDelay: (idx) => idx * 10,
          },
        ],
        color: $event.color ,
        animationEasing: 'elasticOut',
      };

    });


    const convert = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'CFA', });

    this.receiptsService.getDetailsArticle($event.data.name).subscribe((data: any) => {
      this.listReceiptBySousParagraph2 = data.receiptBySousParagraphe;
      this.listReceiptBySousParagraph2.forEach((obj: any) => {
        obj.value = convert.format(Number(obj.value ));
      });
    });
  }



}
