import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpenseComponent } from './expense.component';
import {ExpenseRoutingModule} from './expense-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {NgxEchartsModule} from 'ngx-echarts';

@NgModule({
  imports: [
    CommonModule,
    ExpenseRoutingModule,
    SharedModule,
    NgxEchartsModule
  ],
  declarations: [ExpenseComponent]
})
export class ExpenseModule { }
