import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ExpensesService} from '../../../services/expenses.service';
import {NgxUiLoaderService} from 'ngx-ui-loader';
import {ThemeOption} from 'ngx-echarts';

@Component({
  selector: 'app-radial',
  templateUrl: './expense.component.html',
  styleUrls: [
    './expense.component.scss',
    '../../../../assets/charts/radial/css/radial.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class ExpenseComponent implements OnInit {


  // entity variables
  public entities: [];
  public entities2: [];

  public entity: any;
  public entity2: any;
  public entityName: string;
  public sumExpense: any;
  public percentage: any;
  public showChartExpenseAllEntities: boolean;

  // list expense variables
  public expenseByNature: [];
  public expenseByNature2: any;

  // chart variables
  public chartExpenseBySsNature: {};
  public chartExpenseAllEntities: {};
  public theme: string | ThemeOption;

  // columnDefs
  columnDefs = [
    { field: 'name', sortable: true, filter: true , width: 800, suppressSizeToFit: true   },
    { field: 'value', sortable: true, filter: true  },
  ];

  // gridOptions
  gridOptions = {
    rowClass: 'my-green-class',
    getRowClass: params => {
      if (params.node.rowIndex % 2 === 0) {
        return 'my-shaded-effect';
      }
    },
  };

  constructor(private expensesService: ExpensesService, private ngxService: NgxUiLoaderService) {
  }


  ngOnInit(): void {

    this.buildChartExpenseAllEntities();

  }

  /**
   * buildChartExpenseAllEntities
   *
   */
  buildChartExpenseAllEntities(): void {
    this.ngxService.start();
    this.chartExpenseAllEntities = {};
    const data1 = [];
    const xAxisData = [];

    // show chart
    this.showChartExpenseAllEntities = true ;

    // build all entities
    this.expensesService.getAllEntities().subscribe((data: any) => {
      this.ngxService.stop();
      this.entities = data ;
      this.entities.forEach((obj: any) => {

        obj.entityName = obj.entity;

        // for chart
        xAxisData.push(obj.entityName);
        data1.push(obj.percentage);

        this.chartExpenseAllEntities = {
          legend: {
            data: ['DEPENSE / INSTITUTION'],
            align: 'left',
          },
          tooltip: {},
          xAxis: {
            data: xAxisData,
            silent: false,
            splitLine: {
              show: false,
            },
          },
          yAxis: {},
          series: [
            {
              name: 'DEPENSE / INSTITUTION',
              type: 'bar',
              data: data1,
              animationDelay: (idx) => idx * 10,
            },
          ],
          animationEasing: 'elasticOut',
        };

      });
    }) ;
  }

  /**
   * findEntityExpenses
   *
   */
  findEntityExpenses(entity: string): void {

    this.ngxService.start();
    this.showChartExpenseAllEntities = false;

    this.expensesService.getAllExpensesByEntity(entity).subscribe((data: any) => {
      this.ngxService.stop();
      this.entity = data;
      const listNature = [];
      const listExpense = [];
      const convert = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'CFA', });

      this.expenseByNature = this.entity.expenseByNature;

      this.expenseByNature.forEach((obj: any) => {
        listNature.push(obj.name);
        listExpense.push(obj.value);
      });

      // build entity
      this.entityName = this.entity.entity;
      this.sumExpense = convert.format(Number(this.entity.sumExpense));
      this.percentage = this.entity.percentage;

      this.chartExpenseBySsNature = {

        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
          x: 'center',
          y: 'bottom',
          data: listNature
        },
        calculable: true,
        series: [
          {
            name: 'NATURE:',
            type: 'pie',
            radius: [30, 110],
            roseType: 'area',
            data: this.expenseByNature
          }
        ]
      };

    });


    this.expensesService.getAllExpensesByEntity(entity).subscribe((data: any) => {

      this.entity = data;
      const convert = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'CFA', });
      this.expenseByNature2 = this.entity.expenseByNature;
      this.expenseByNature2.forEach((obj: any) => {
        obj.value = convert.format(Number(obj.value));
      });

    });

  }
}
