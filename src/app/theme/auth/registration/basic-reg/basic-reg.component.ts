import { Component, OnInit } from '@angular/core';
import {User} from '../../../../models/user';
import {AuthService} from '../../../../services/auth.service';
import {TokenStorageService} from '../../../../services/token-storage.service';
import {Router} from '@angular/router';
import {UserService} from '../../../../services/user.service';

@Component({
  selector: 'app-basic-reg',
  templateUrl: './basic-reg.component.html',
  styleUrls: ['./basic-reg.component.scss']
})
export class BasicRegComponent implements OnInit {

  signInPage = false;
  signUpPage = false ;
  isLoggedIn = false ;

  errorMessage = '';
  successMessage = ''  ;

  roles: string[];
  login: any;
  password2: any;
  userSave: User;
  email: string;
  active: string;
  password: string;
  checkDataBeforeSave: boolean ;
  checked: false;

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService ,
    private router: Router,
    private userService: UserService) { }

  ngOnInit(): void {
    this.signInPage = true ;
    this.userSave = new User();
    this.roles = new Array();
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
    document.querySelector('body').setAttribute('themebg-pattern', 'theme6');
  }

  /**
   * createUser
   *
   */
  createUser(): void  {

    this.successMessage = '';
    this.errorMessage = '';

    this.checkBeforeSave(this.userSave);

    if (this.checkDataBeforeSave) {

      //  this.ngxService.start();
      this.userService.createUser(this.userSave).subscribe((data: any) => {
          //  this.ngxService.stop();

          if (data.active === false) {
            this.successMessage = 'The user is successfully created';
            this.userSave = new User();
            this.password2 = '';
          }
        },
        err => {
          console.log(err);
          // this.ngxService.stop();
          this.errorMessage = 'The user already exists in the database';
          console.log(this.errorMessage);
        }
      );

    }

  }

  /**
   * checkBeforeSave
   *
   */
  checkBeforeSave(user: User): void  {

    if (user.login === undefined || user.login === null || user.login === '') {
      this.errorMessage = 'Login must not be empty ';
      this.checkDataBeforeSave = false ;
      return ;
    }

    if (user.email === undefined || user.email === null || user.email === '') {
      this.errorMessage = 'Email must not be empty ';
      this.checkDataBeforeSave = false ;
      return ;
    }

    if (user.password === undefined || user.password === null || user.password === '') {
      this.errorMessage = 'Password must not be empty';
      this.checkDataBeforeSave = false ;
      return ;
    }

    if (user.password !== this.password2) {
      this.errorMessage = 'The password is incorrect ';
      this.checkDataBeforeSave = false ;
      return;
    }
    if (!this.checked) {
      this.errorMessage = 'You need to read and accept Terms & Conditions. ';
      this.checkDataBeforeSave = false ;
      return;
    }
    this.errorMessage = '';
    this.checkDataBeforeSave = true;
  }


}
