import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Recette & Dépense',
      status: false
    },
    children: [
      {
        path: 'recipe-add',
        loadChildren: () => import('./recipe-add/recipe-add.module').then(m => m.RecipeAddModule)
      },
      {
        path: 'recipe',
        loadChildren: () => import('./recipe/recipe.module').then(m => m.RecipeModule)
      },
      {
        path: 'expense',
        loadChildren: () => import('./expense/expense.module').then(m => m.ExpenseModule)
      },
      {
        path: 'expense-add',
        loadChildren: () => import('./expense-add/expense-add.module').then(m => m.ExpenseAddModule)
      },
      {
        path: 'chart-js',
        loadChildren: () => import('./chart-js/chart-js.module').then(m => m.ChartJsModule)
      },
      {
        path: 'radial',
        loadChildren: () => import('./radial/radial.module').then(m => m.RadialModule)
      },
      {
        path: 'c3-js',
        loadChildren: () => import('./c3-js/c3-js.module').then(m => m.C3JsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseRecipeRoutingModule { }
