import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpenseAddComponent } from './expense-add.component';
import {ExpenseAddRoutingModule} from './expense-add-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {SelectModule} from 'ng-select';
import {FormsModule} from '@angular/forms';
import {ToastyModule} from 'ng2-toasty';

@NgModule({
  imports: [
    CommonModule,
    ExpenseAddRoutingModule,
    SharedModule,
    Ng2GoogleChartsModule,
    SelectModule,
    FormsModule,
    ToastyModule.forRoot()
  ],
  declarations: [ExpenseAddComponent]
})
export class ExpenseAddModule { }
