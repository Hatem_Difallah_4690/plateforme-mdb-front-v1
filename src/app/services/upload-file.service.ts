import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UploadFileService {

    private baseUrl = 'http://localhost:8080/api/v0/import';

    constructor(private http: HttpClient) { }

    upload(file: File , typeFile: string ): Observable<HttpEvent<any>> {
        const formData: FormData = new FormData();

        const callUrl = typeFile === 'RECETTE_LFI' ? `${this.baseUrl}/receipt` : `${this.baseUrl}/expense` ;

        formData.append('file', file);
        const req = new HttpRequest('POST', callUrl, formData, {
            reportProgress: true,
            responseType: 'json'
        });

        return this.http.request(req);
    }

    findAll(): Observable<any> {
        return this.http.get(`${this.baseUrl}/receipts` , { responseType: 'json' });
    }

    deletAll(): Observable<any> {
        return this.http.get(`${this.baseUrl}/deletAll`);
    }
    getFiles(): Observable<any> {
        return this.http.get(`${this.baseUrl}/files`);
    }
}
