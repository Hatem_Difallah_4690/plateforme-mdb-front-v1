import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BudgetComponent} from './budget.component';
import {BudgetRoutingModule} from './budget-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';
import {AgmCoreModule} from '@agm/core';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';

@NgModule({
  imports: [
    CommonModule,
    BudgetRoutingModule,
    SharedModule,
    ChartModule,
    Ng2GoogleChartsModule,
    /*SimpleNotificationsModule.forRoot(),*/
    AgmCoreModule.forRoot({apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk'})
  ],
  declarations: [
    BudgetComponent,
  ],
  bootstrap: [BudgetComponent]
})
export class BudgetModule { }
