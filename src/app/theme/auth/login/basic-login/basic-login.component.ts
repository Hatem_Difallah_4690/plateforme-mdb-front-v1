import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../services/auth.service';
import {TokenStorageService} from '../../../../services/token-storage.service';
import {Router} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {User} from '../../../../models/user';


@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {


  signInPage = false;
  signUpPage = false ;
  forgotPage = false ;
  isLoggedIn = false ;

  errorMessage = '';
  successMessage = ''  ;

  roles: string[];
  login: any;
  password2: any;
  userSave: User;
  email: string;
  active: string;
  password: string;
  checkDataBeforeSave: boolean ;

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService ,
    private router: Router,
    private userService: UserService) { }

  /**
   * ngOnInit
   *
   */
  ngOnInit(): void {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme6');

    this.signInPage = true ;
    this.userSave = new User();
    this.roles = new Array();
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }


  /**
   * onSubmit
   *
   */
  onSubmit(): void  {
    const user = new User();
    user.login = this.login;
    user.password = this.password;
    this.userService.authentication(user).subscribe(
      data => {
        if (data.logon) {
          console.log(data.authorizationDtos);
          sessionStorage.setItem('isLoggedin', 'true');
          this.router.navigate(['/dashboard/budget']);
          data.authorizationDtos.forEach(e => {
            this.roles.push(e.libelle) ;
          });
          sessionStorage.setItem('roles', this.roles.toString());
          sessionStorage.setItem('userLogin' , data.login) ;
        } else {
          this.errorMessage = 'Authentication failed (username or password incorrect)' ;
        }
      },
      err => {
        console.log(err);
      }
    );

  }


}
