import { Component, OnInit } from '@angular/core';
import {ReceiptsService} from '../../../services/receipts.service';
import {Reciept} from '../../../models/reciept';
import {ToastyService} from 'ng2-toasty';

@Component({
  selector: 'app-google-chart',
  templateUrl: './expense-add.component.html',
  styleUrls: ['./expense-add.component.scss']
})
export class ExpenseAddComponent implements OnInit {
  paragraphes: any ;
  sousParagraphes: any ;
  rubrics: any;
  sousRubrics: any;
  repartitions: any;
  receipt: Reciept;
  checkDataBeforeSave: boolean ;
  private errorMessage: string;

  constructor( private  receiptsService: ReceiptsService ,
               private toastyService: ToastyService

  ) { }

  ngOnInit(): void {

    this.receipt = new Reciept();
  }

  /**
   * buildParagraphes
   *
   */
  buildParagraphes(target): void  {
    this.receiptsService.findParagraphesByArticle(target.value).subscribe((data: any) => {
      this.paragraphes = data;
    });
  }

  /**
   * buildSousParagraphes
   *
   */
  buildSousParagraphes(target): void {
    this.receiptsService.findSousParagraphesByParagraphe(target.value).subscribe((data: any) => {
      this.sousParagraphes = data;
    });
  }

  /**
   * buildRubrics
   *
   */
  buildRubrics(target): void  {
    this.receiptsService.findRubricsBySousParagraphe(target.value).subscribe((data: any) => {
      this.rubrics = data;
    });
  }

  /**
   * buildSousRubrics
   *
   */
  buildSousRubrics(target): void  {
    this.receiptsService.findSousRubricsByRubric(target.value).subscribe((data: any) => {
      this.sousRubrics = data;
    });
  }

  /**
   * buildRepartitions
   *
   */
  buildRepartitions(target): void  {
    this.receiptsService.findRepartitionsBySousRubric(target.value).subscribe((data: any) => {
      this.repartitions = data;
    });
  }

  /***
   * createReceipt
   *
   */
  createReceipt(): void  {

    this.checkBeforeSave(this.receipt);

    if (this.checkDataBeforeSave) {
      this.receiptsService.createReceipt(this.receipt).subscribe((data: any) => {
        console.log(data);
        this.showSuccess('Success', 'Recipe : ' + data.libelle + ' successfully added ');
        this.receipt = new Reciept();
      });
    }else {
      this.showError('Error', this.errorMessage);

    }
  }

  /**
   * showSuccess
   *
   */
  showSuccess(title , msg): void {
    this.toastyService.success(
      { title , msg   , timeout: 5000 , showClose: true , theme: 'bootstrap'
      });
  }

  /**
   *
   */
  showError(title , msg): void {
    this.toastyService.error(
      { title , msg   , timeout: 5000 , showClose: true , theme: 'bootstrap'
      });
  }


  close(): void {
    this.receipt = new Reciept();
  }


  checkBeforeSave(receipt: Reciept): void  {

    console.log(receipt);
    if (receipt.libelle === undefined || receipt.libelle === null || receipt.libelle === '') {
      this.errorMessage = 'The name for receipt must not be empty ';
      this.checkDataBeforeSave = false ;
      return ;
    }

    if (receipt.amount === undefined || receipt.amount === null || receipt.amount === '') {
      this.errorMessage = 'The amount for receipt must not be empty ';
      this.checkDataBeforeSave = false ;
      return ;
    }

    this.errorMessage = '';
    this.checkDataBeforeSave = true;
  }

}
