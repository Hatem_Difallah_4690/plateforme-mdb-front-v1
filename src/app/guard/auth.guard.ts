import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) {}

    canActivate(): boolean  {
        if (sessionStorage.getItem('isLoggedin')) {
            return true;
        }
        this.router.navigate(['/auth/login/simple']);
        return false;
    }


  isLogged(): string  {
    return sessionStorage.getItem('isLoggedin');
  }
}
