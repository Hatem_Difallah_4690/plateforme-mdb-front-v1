import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserManageComponent} from './user-manage.component';

const routes: Routes = [
  {
    path: '',
    component: UserManageComponent,
    data: {
      title: 'Gestion des utilisateurs',
      icon: 'icon-user',
      caption: 'Cette page est utilisée pour gérer l\'utilisateur - gestion des utilisateurs',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserManageRoutingModule { }
