import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FileUploader} from 'ng2-file-upload';
import {Observable} from 'rxjs/Observable';
import {UploadFileService} from '../../../services/upload-file.service';
import {HttpEventType, HttpResponse} from '@angular/common/http';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-file-upload-ui',
  templateUrl: './file-upload-ui.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./file-upload-ui.component.scss']
})
export class FileUploadUiComponent implements OnInit {
  uploader: FileUploader = new FileUploader({
    url: URL,
    isHTML5: true
  });
  hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;
  typeFile: any;
  selectedFiles?: FileList;
  progressInfos: any[] = [];
  message: string[] = [];
  selctedFileName: string;
  fileInfos?: Observable<any>;



  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  constructor(private uploadService: UploadFileService) { }

  ngOnInit(): void {
  }

  selectFiles(event: any): void {
    this.message = [];
    this.progressInfos = [];
    this.selectedFiles = event.target.files;
    this.selctedFileName = event.target.files.length > 0 ? event.target.files[0].name : 'Chose file'  ;
  }

  upload(idx: number, file: File , typeFile: string): void {
    this.progressInfos[idx] = { value: 0, fileName: file.name };

    if (file) {
      this.uploadService.upload(file , typeFile).subscribe(
        (event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            const msg = 'Uploaded the file successfully: ' + file.name;
            this.message.push(msg);
            this.fileInfos = this.uploadService.getFiles();
          }
        },
        (err: any) => {
          this.progressInfos[idx].value = 0;
          const msg = 'Could not upload the file: ' + file.name;
          this.message.push(msg);
          this.fileInfos = this.uploadService.getFiles();
        });
    }
  }

  uploadFiles(): void {

    console.log(this.typeFile);
    this.message = [];

    if (this.selectedFiles) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        this.upload(i, this.selectedFiles[i] , this.typeFile);
      }
    }
  }
}
