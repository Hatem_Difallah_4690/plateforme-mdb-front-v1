import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ExpenseRecipeRoutingModule} from './expense-recipe-routing.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ExpenseRecipeRoutingModule,
    SharedModule
  ],
  declarations: []
})
export class ExpenseRecipeModule { }
